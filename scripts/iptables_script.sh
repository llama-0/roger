#!/bin/bash

# iptables config script

IPT=iptables

# flush current rules from iptables

$IPT -F
$IPT -X
$IPT -t nat -F
$IPT -t nat -X
$IPT -t mangle -F
$IPT -t mangle -X

# Drop invalid packets beforehand

$IPT -t mangle -A PREROUTING -m conntrack --ctstate INVALID -j DROP

# Drop TCP packets that are new and aren't SYN

$IPT -t mangle -A PREROUTING -p tcp ! --syn -m conntrack --ctstate NEW -j DROP

# Drop SYN packets with uncommon MSS (Maximum Segment Size) value (default : max possible)

$IPT -t mangle -A PREROUTING -p tcp -m conntrack --ctstate NEW -m tcpmss ! --mss 536:65535 -j DROP

# Drop (bogus packets) all combinations of tcp-flags that should never occur naturally (including NULL packets)

$IPT -t mangle -A PREROUTING -p tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG NONE -j DROP
$IPT -t mangle -A PREROUTING -p tcp --tcp-flags FIN,SYN FIN,SYN -j DROP
$IPT -t mangle -A PREROUTING -p tcp --tcp-flags SYN,RST SYN,RST -j DROP
$IPT -t mangle -A PREROUTING -p tcp --tcp-flags FIN,RST FIN,RST -j DROP
$IPT -t mangle -A PREROUTING -p tcp --tcp-flags FIN,ACK FIN -j DROP
$IPT -t mangle -A PREROUTING -p tcp --tcp-flags ACK,URG URG -j DROP
$IPT -t mangle -A PREROUTING -p tcp --tcp-flags ACK,FIN FIN -j DROP
$IPT -t mangle -A PREROUTING -p tcp --tcp-flags ACK,PSH PSH -j DROP
$IPT -t mangle -A PREROUTING -p tcp --tcp-flags ALL ALL -j DROP
$IPT -t mangle -A PREROUTING -p tcp --tcp-flags ALL NONE -j DROP
$IPT -t mangle -A PREROUTING -p tcp --tcp-flags ALL FIN,PSH,URG -j DROP
$IPT -t mangle -A PREROUTING -p tcp --tcp-flags ALL SYN,FIN,PSH,URG -j DROP
$IPT -t mangle -A PREROUTING -p tcp --tcp-flags ALL SYN,RST,ACK,FIN,URG -j DROP

# Set default chain policies 

$IPT -P INPUT DROP
$IPT -P OUTPUT DROP
$IPT -P FORWARD DROP

# pass all that goes through established connection

$IPT -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT

# Allow loopback access

$IPT -A INPUT -i lo -j ACCEPT
$IPT -A OUTPUT -o lo -j ACCEPT

# allow output from Internet connection interface

$IPT -A OUTPUT -o enp0s3 -j ACCEPT

# Allow some icmp ping

$IPT -A INPUT -p icmp --icmp-type echo-request -j ACCEPT
$IPT -A INPUT -p icmp --icmp-type time-exceeded -j ACCEPT
$IPT -A INPUT -p icmp --icmp-type destination-unreachable -j ACCEPT

# open SSH connection

$IPT -A INPUT -m conntrack --ctstate NEW -p tcp --dport 38456 -j ACCEPT

# open ports: http,https

$IPT -A INPUT -m conntrack --ctstate NEW -p tcp -m multiport --dports 80,443 -j ACCEPT

# open ports: smtp,smtps (Simple Mail Transfer Protocol). To send mail across the Internet.

$IPT -A INPUT -m conntrack --ctstate NEW -p tcp -m multiport --dports 25,465 -j ACCEPT

# open ports: POP3,POP3S (Post Office Protocol version 3). To receive mail froma  remote server to a local mail client.

$IPT -A INPUT -m conntrack --ctstate NEW -p tcp -m multiport --dports 110,995 -j ACCEPT

# open ports: IMAP,IMAPS (Internet Message Access Protocol). Access mail on a remote web server from a local client.

$IPT -A INPUT -m conntrack --ctstate NEW -p tcp -m multiport --dports 143,993 -j ACCEPT

# open port for dns

$IPT -A INPUT -m conntrack --ctstate NEW -p udp --dport 53 -j ACCEPT

# add fail2ban rules

systemctl restart fail2ban

# write and save rules

/sbin/iptables-save > /etc/sysconfig/iptables
